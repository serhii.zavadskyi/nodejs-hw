const express = require('express');
const morgan = require('morgan')
const filesRouter = require('./filesRouter')

const app = express();
const port = 8080;

app.use(morgan('tiny'))
app.use(express.json());
app.use(filesRouter);

app.all('*', (req, res) => {
    throw new Error("Bad request");
})

app.use((err, req, res, next) => {
    if (err.message === "Bad request") {
        res.status(400).json({  
            message: "Client error"
          }); 
    }
}); 

app.listen(port,  () => {
    console.log(`App listening at http://localhost:${port}`)
})