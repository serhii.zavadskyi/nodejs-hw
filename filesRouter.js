const express = require('express');
const fs = require('fs');
const router = express.Router();
const dir = './files';

const validFormats = ['.log', '.txt', '.json', '.yaml', '.xml', '.js'];
const isValidFormat = (str) => validFormats.some(format => str.endsWith(format))

router.post('/api/files', (req, res) => {

    if (!fs.existsSync(dir)){
        fs.mkdirSync(dir);
    }
 
    const { filename, content } = req.body;

    if (filename === undefined) {
        res.status(400).json({ message: "Please specify 'filename' parameter"})
    }

    if (content === undefined) {
        res.status(400).json({ message: "Please specify 'content' parameter"})
    }

    if (!isValidFormat(filename) ||!content) {
        res.status(400).json({ message: "No valid data"})
    }

    if (isValidFormat(filename)) {

        fs.writeFile(`files/${filename}`, content, (err) => {

            if (err) {
                res.status(500).json({
                    message: "Server error"
                })
            };

            res.status(200).json({ message: "File created successfully" })
        });
    }
})

router.get('/api/files', (req, res) => {

    fs.readdir('files', (err, files) => {

        if (err) {
            res.status(400).json({ message: `No files uploaded yet` });
        }

        res.status(200).json({
            message: "Success", 
            files
        })
    });
})

router.get('/api/files/:filename', (req, res) => {

    const { filename } = req.params;

    fs.readFile(`files/${filename}`, 'utf8', (err, content) => {

        if (!isValidFormat(filename)) {
            res.status(400).json({ message: "Please specify valid format" })
        }

        if (err) {
            res.status(400).json({ message: `No file with ${filename} filename found` })
        }

        const extension = filename.split('.').pop();

        fs.stat(`files/${filename}`, (err, stats) => {
            if(err){
                console.log(err);
            }

            res.status(200).json({
                message: "Success",
                filename,
                content,
                extension,
                uploadedDate: stats.mtime
            }) 
        }) 
    });
})

module.exports = router;